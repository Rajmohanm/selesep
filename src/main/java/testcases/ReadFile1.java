package testcases;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadFile1 {
	//public static Object[][] getSheet(String dataSheetname)
	//@Test
	//in order to return the xl file values to the program we need to read the excel
		//value and store it on Object Array and return the object array to the program
		public static Object[][] dataRead(String dataSheetName, int x) throws IOException {

			XSSFWorkbook wb = new XSSFWorkbook("./data/"+dataSheetName+".xlsx");
			XSSFSheet sheet = wb.getSheetAt(x);
			int rowcount = sheet.getLastRowNum();
			System.out.println(rowcount);
			int cellcount = sheet.getRow(0).getLastCellNum();
			System.out.println(cellcount);
			Object data[][] = new Object[rowcount][cellcount];
			for (int j = 1; j<=rowcount; j++) {
				XSSFRow row = sheet.getRow(j);
				System.out.println("---------------------------");
				for (int i = 0; i < cellcount; i++) {
					XSSFCell cell = row.getCell(i);
					try {
						String value = cell.getStringCellValue();				
						System.out.println(value);
						data[j-1][i]=value;
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						System.out.println(" ");
					}
					
				} 
				
			}

			wb.close();
			return data;

		}
}
