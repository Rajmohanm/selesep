package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

//import wdMethods.ProjectMethodsCL;
import wdMethods.ProjectMethodsPOM;

public class HomePage extends ProjectMethodsPOM{
	
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	//@FindBy(how=How.CLASS_NAME,using ="decorativeButton") WebElement eleLogin;
	@FindBy(how=How.LINK_TEXT,using="CRM/SFA") WebElement eleCRM;
	public MyHome clickCRMLink()
	{
		click(eleCRM);
		return new MyHome();
		
	}
}
