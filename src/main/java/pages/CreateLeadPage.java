package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

//import wdMethods.ProjectMethodsCL;
import wdMethods.ProjectMethodsPOM;

public class CreateLeadPage extends ProjectMethodsPOM{
	
	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using ="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.ID,using ="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID,using ="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.ID,using ="createLeadForm_primaryEmail") WebElement eleMailID;
	@FindBy(how=How.ID,using ="createLeadForm_primaryPhoneNumber") WebElement elePhNum;
	//@FindBy(how=How.CLASS_NAME,using ="decorativeButton") WebElement eleLogin;
	@FindBy(how=How.NAME,using="submitButton") WebElement eleSubmit;

	
	public CreateLeadPage enterCompanyName(String cName)
	{
		//WebElement eleUserName = locateElement("id","username");
		type(eleCompanyName,cName);
		return this;
	}
	public CreateLeadPage enterFirstName(String fName)
	{
		//WebElement eleUserName = locateElement("id","username");
		type(eleFirstName,fName);
		return this;
	}
	
	public CreateLeadPage enterLastName(String lName)
	{
		//WebElement eleUserName = locateElement("id","username");
		type(eleLastName,lName);
		return this;
	}
	public CreateLeadPage enterMailID(String mailID)
	{
		//WebElement eleUserName = locateElement("id","username");
		type(eleMailID,mailID);
		return this;
	}
	
	public CreateLeadPage enterPhoneNumber(String phNum)
	{
		//WebElement eleUserName = locateElement("id","username");
		type(elePhNum,phNum);
		return this;
	}
	
	public ViewLeadsPage clickSubmit()
	{
		click(eleSubmit);
		return new ViewLeadsPage();
		
	}


}
