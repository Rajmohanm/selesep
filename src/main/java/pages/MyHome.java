package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

//import wdMethods.ProjectMethodsCL;
import wdMethods.ProjectMethodsPOM;

public class MyHome extends ProjectMethodsPOM{
	public MyHome()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.LINK_TEXT,using="Leads") WebElement eleLead;
	//@FindBy(how=How.LINK_TEXT,using="Leads") WebElement eleCreateLead;
	
	public MyLeadsPage clickLead()
	{
		click(eleLead);
		return new MyLeadsPage();
		
	}
	
	
}
