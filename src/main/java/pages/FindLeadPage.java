package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethodsPOM;

public class FindLeadPage extends ProjectMethodsPOM{
	public FindLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.NAME,using ="firstName") WebElement eleFirstName;
	@FindBy(how=How.XPATH,using="//button[contains(text(),'Find Leads')]") WebElement eleFindButton;
	@FindBy(how=How.XPATH,using="(//a[@class='linktext'])[1]") WebElement eleFirstID;
	
	public FindLeadPage enterFirstName(String Fname)
	{
		type(eleFirstName,Fname);
		return this;
	}
	public FindLeadPage clickFindButton() throws InterruptedException
	{
		click(eleFindButton);
		Thread.sleep(2000);
		
		return this;		
    }
	public MergeLeadPage clickFirstID()
	{
		clickWithNoSnap(eleFirstID);
		
		switchToWindow(0);
	
		return new MergeLeadPage();
	
    }
	
}
