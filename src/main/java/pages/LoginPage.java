package pages;

//import org.apache.poi.ss.usermodel.PageOrder;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

//import wdMethods.ProjectMethodsCL;
import wdMethods.ProjectMethodsPOM;

public class LoginPage extends ProjectMethodsPOM{
	
	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using ="username") WebElement eleUserName;
	@FindBy(how=How.ID,using ="password") WebElement elePassWord;
	@FindBy(how=How.CLASS_NAME,using ="decorativeSubmit") WebElement eleLogin;

	
	public LoginPage enterUserName(String uname)
	{
		//WebElement eleUserName = locateElement("id","username");
		type(eleUserName,uname);
		return this;
	}
	public LoginPage enterPassword(String pass)
	{
		//WebElement elePassWord = locateElement("id","password");
		type(elePassWord,pass);
		return this;
	}
	
	public HomePage clickLogin()
	{
		click(eleLogin);
		return new HomePage();
		
	}
	
	

}
