package pages;


import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethodsPOM;

public class MergeLeadPage extends ProjectMethodsPOM{
	
	public MergeLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.XPATH,using="(//img[@src='/images/fieldlookup.gif'])[1]") WebElement eleFindLead;
	@FindBy(how=How.NAME,using ="firstName") WebElement eleFirstName;
	@FindBy(how=How.XPATH,using="//button[contains(text(),'Find Leads')]") WebElement eleFindButton;
	@FindBy(how=How.XPATH,using="(//a[@class='linktext'])[1]") WebElement eleFirstID;
	@FindBy(how=How.XPATH,using="(//img[@src='/images/fieldlookup.gif'])[2]") WebElement eleFindLead1;
	@FindBy(how=How.XPATH,using="//a[text()='Merge']") WebElement eleMerge;
	public FindLeadPage clickFindLead()
	{
		click(eleFindLead);
		switchToWindow(1);
		return new FindLeadPage();
		
	}
	public FindLeadPage clickFindLead1()
	{
		click(eleFindLead1);
		switchToWindow(1);
		return new FindLeadPage();
		
	}
	public MergeLeadPage clickMerge() throws InterruptedException
	{
		click(eleMerge);
		Alert ale = driver.switchTo().alert();
        String str = ale.getText();
        System.out.println(str);
        Thread.sleep(1000);          
        ale.accept();
		//switchToWindow(1);
		return this;
		
	}
	
	/*public MergeLeadPage alertAccept() throws InterruptedException
	{
		Thread.sleep(2000);
		Alert ale = driver.switchTo().alert();
        String str = ale.getText();
        System.out.println(str);
        Thread.sleep(2000);          
        ale.accept();
        return this;

	}
*/	/*public MergeLeadPage enterFirstName(String Fname)
	{
		type(eleFirstName,Fname);
		return this;
	}
	public MergeLeadPage clickFindButton() throws InterruptedException
	{
		click(eleFindButton);
		Thread.sleep(2000);
		
		return this;		
    }
	public MergeLeadPage clickFirstID()
	{
		clickWithNoSnap(eleFirstID);
		
		switchToWindow(0);
	
		return new MergeLeadPage();
	
    }
	public MergeLeadPage clickFindLead1()
	{
		click(eleFindLead1);
		switchToWindow(1);
		return this;
		
	}
*/	
	
	}

