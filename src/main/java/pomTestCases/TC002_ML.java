package pomTestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.FindLeadPage;
import pages.LoginPage;
import wdMethods.ProjectMethodsPOM;

public class TC002_ML extends ProjectMethodsPOM{
	@BeforeTest
	public void setData()
	{
		testCaseName = "TC001_CreateLead";
		testDesc = "Create a new Lead";
		author = "Rajmohan";
		category = "smoke";
		data= "AllFive";

	}
	@Test(dataProvider="cl")
	public void Mergelead(String uName, String password,String cname,String fname,String lname,String email,String phn ) throws InterruptedException
	{
		new LoginPage()
		.enterUserName(uName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMLink()
		.clickLead()
		.clickMergeLead()
		.clickFindLead()
		
		.enterFirstName(fname)
		.clickFindButton()
		.clickFirstID()
		.clickFindLead1()
		.enterFirstName(fname)
		.clickFindButton()
		.clickFirstID()
		.clickMerge();
		
		//.alertAccept();
		
		//read.sleep(2000);
		//.switchToWindow(0)
//		;
		
		//.clickFindLead();
		
		Thread.sleep(2000);
		
}
}
