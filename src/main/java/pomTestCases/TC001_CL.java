package pomTestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethodsPOM;


public class TC001_CL extends ProjectMethodsPOM{
	@BeforeTest
	public void setData()
	{
		testCaseName = "TC001_CreateLead";
		testDesc = "Create a new Lead";
		author = "Rajmohan";
		category = "smoke";
		data= "AllFive-POM";
		x=0;

	}
	@Test(dataProvider="cl")
	public void createlead(String uName, String password,String cname,String fname,String lname,String email,String phn ) throws InterruptedException
	{
		new LoginPage()
		.enterUserName(uName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMLink()
		.clickLead()
		.clickCreateLead()
		.enterCompanyName(cname)
		.enterFirstName(fname)
		.enterLastName(lname)
		.enterMailID(email)
		.enterPhoneNumber(phn)
		.clickSubmit();
		Thread.sleep(2000);
		
	}

}
