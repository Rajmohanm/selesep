package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import testcases.ReadFile;
import testcases.ReadFile1;

public class ProjectMethodsPOM extends SeMethods{
	
	//public String dataSheetname;
	public String data;
	public int x;


	@BeforeSuite
	public void beforeSuite() {
		startResult();
	}
	@BeforeMethod
	public void login() throws InterruptedException {
	    beforeMethod();
		startApp("chrome", "http://leaftaps.com/opentaps");
			
	}
	@AfterMethod
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite
	public void afterSuite() {
		endResult();
	}
	
	
	@DataProvider(name="cl")
public Object[][] fetchdata() throws IOException
	{
		//int x=0;
		//ReadFile rf= new ReadFile();
	    return ReadFile1.dataRead(data,x);
		//Object[][] data=readFile2.dataRead(x);
     	//Object[][] data=ReadFile.dataRead(x);
		//return 
	}

}






